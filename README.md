### About
This addon adds the possibility to control an armature through [OpenPose](https://github.com/CMU-Perceptual-Computing-Lab/openpose), a library which does keypoint detection for body, face, hands and feet.

### License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version (http://www.gnu.org/licenses/).

### Authors
* Marie-Ève Dumas
* Emmanuel Durand

### Project URL
This project can be found on the [SAT Metalab repository](https://gitlab.com/sat-metalab/blender-add-openpose)

# Requirements
OpenPose is quite resource-heavy library. For face detection it needs at the very least a GPU with 2GB of memory. For body detection it needs at least 6GB of memory.

This addons needs [OpenPose](https://github.com/CMU-Perceptual-Computing-Lab/openpose) to be installed on your system.

### Installation
This addon can be installed in two ways:
* if you are a developer, you can install a symbolic link to your Blender addon directory with the _install.sh_ script.
* if you are a user, you can "build" the addon with the _build.sh_ script, then install the resulting _blender-openpose.zip_ file like any other Blender addon

Commit ID d7ae8dd9 tested and fully funtional.

### Sponsors
This project is made possible thanks to the [Society for Arts and Technologies](http://www.sat.qc.ca) (also known as SAT).
Thanks to the Ministère de l'Économie et de l'Innovation du Québec (MEI).
