Installing the OpenPose addon for Blender
=========================================

# Requirements

OpenPose is a very demanding library and needs an up-to-date GPU. This guide will explain step-by-step how to install the addon on a computer with a Nvidia GPU, although using an AMD GPU should not be that much different.

Note also that this guide considers that the operating system is Ubuntu 19.04 or newer, and the whole process may not work on a previous release (not to mention a different distribution).


# Installation

In the following, it will be considered that the repositories are cloned into `${HOME}/src`

## Dependencies

```bash
sudo apt install nvidia-cuda-dev nvidia-cuda-toolkit nvidia-dkms-418 nvidia-driver-418 nvidia-kernel-common-418 nvidia-kernel-source-418 nvidia-compute-utils-418 nvidia-settings nvidia-utils-418
sudo apt install python3-pip libboost-all-dev python3-numpy git build-essential cmake libgoogle-glog-dev libprotobuf-dev libopencv-dev protobuf-compiler libatlas-base-dev python3-opencv libhdf5-dev

# Blender dependencies
sudo add-apt-repository ppa:thomas-schiex/blender
sudo apt install git build-essential libx11-dev libxi-dev libxrender-dev libsndfile1-dev libopenexr-dev libopenjp2-7-dev libpng-dev libjpeg-dev libopenal-dev libalut-dev python3.7-dev libglu1-mesa-dev libsdl-dev libfreetype6-dev libtiff5-dev libavdevice-dev libavformat-dev libavutil-dev libavcodec-dev libswscale-dev libx264-dev libxvidcore-dev libmp3lame-dev libspnav-dev python3.7 libboost-all-dev libopenimageio-dev libopencolorio-dev libgoogle-glog-dev
```

## OpenPose

To install the OpenPose library, run the following commands:

```bash
git clone https://github.com/CMU-Perceptual-Computing-Lab/openpose
cd openpose
sudo apt install libhdf5-dev
mkdir build && cd build
cmake -DCUDNN_INCLUDE=/usr/local/cuda/include -DCUDNN_LIBRARY=/usr/local/cuda/lib64 -DBUILD_CAFFE=ON -DBUILD_PYTHON=ON ..
make -j$(nproc)
sudo make install && sudo ldconfig
```

If by any chance compilation fails with an error similar to:

```
nvcc fatal   : Unsupported gpu architecture 'compute_20'
```

Then change line 21 of the `openpose/cmake/Cuda.cmake` with:

```
set(Caffe_known_gpu_archs "35 50 52 60 61")  
```

## Blender 2.8

This addon is meant to be used with Blender 2.8. We have to compile it by ourself to make sure to use the system glog library:

```bash
git clone http://git.blender.org/blender.git
cd blender
git submodule update --init --recursive
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release -DWITH_INSTALL_PORTABLE=OFF -DWITH_PYTHON_INSTALL=OFF -DWITH_PYTHON_INSTALL_NUMPY=OFF -DWITH_CODEC_FFMPEG=ON -DWITH_CODEC_SNDFILE=ON -DWITH_SDL=ON -DWITH_JACK=ON -DWITH_OPENCOLORIO=ON -DWITH_OPENVDB=ON -DWITH_ALEMBIC=ON -DWITH_SYSTEM_GLOG=ON ..
make -j$(nproc)
sudo make install
```

We can then install the addon:

```bash
git clone https://gitlab.com/sat-metalab/blender-addon-openpose.git
cd blender-addon-openpose
./install.sh
```

It is now necessary to activate the addon from the Blender user preferences:
- open Blender,
- go to `Edit` then `Preferences`,
- go to `Add-ons`,
- in the drop down menu, select `User`,
- the addon should be in the list, check the tick box to activate it.


# Usage

The OpenPose additions should be visible in the properties of any `Armature` object created. This addon should be used with an armature matching the face output format as defined on the [OpenPose documentation](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#face-output-format). The easiest way to go is to load the rigged face located at `./assets/rigged_face.blend`.
